﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using System.Threading.Tasks;

public class Cutscene : MonoBehaviour
{

    public Text text;
    public Text name;
    public Image imageLeft;
    public Image imageRight;
    public Image background;
    public Sprite s_Rin;
    public Sprite s_Kravan;
    public Sprite s_Volgar;
    public Sprite s_Ozymandias;
    public Sprite s_Volo;
    public Sprite s_Suite;
    public Sprite s_Muelle;
    int counter = 0;
    private string line;
    private string nameLine;
    private System.IO.StreamReader file;
    private System.IO.StreamReader fileNames;
    private Color colorBlack = Color.black;
    private Color colorWhite = Color.white;
    
    // Start is called before the first frame update
    void Start()
    {
        
        StreamWriter sw = new StreamWriter(@"Save.txt");
        sw.Write(SceneManager.GetActiveScene().name);
        sw.Close();
        
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Taberna 1"))
        {
            // Read the file and display it line by line.
            file = new System.IO.StreamReader(@".\Text\Cutscene1.txt");  
            fileNames = new System.IO.StreamReader(@".\Text\NamesCutscene1.txt"); 
            
            line = file.ReadLine();
            nameLine = fileNames.ReadLine();
            text.text = line;
            name.text = nameLine;
            counter++;
        } else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Scene 2"))
        {
            // Read the file and display it line by line.
            file = new System.IO.StreamReader(@".\Text\Cutscene2.txt");  
            fileNames = new System.IO.StreamReader(@".\Text\NamesCutscene2.txt"); 
            
            line = file.ReadLine();
            nameLine = fileNames.ReadLine();
            text.text = line;
            name.text = nameLine;
            counter++;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            line = file.ReadLine();
            nameLine = fileNames.ReadLine();
            switch (nameLine)
            {
                case "Rin":
                    imageLeft.sprite = s_Rin;
                    imageRight.color = Color.black;
                    imageLeft.color = Color.white;
                    break;
                case "Kravan":
                    imageRight.sprite = s_Kravan;
                    imageLeft.color = Color.black;
                    imageRight.color = Color.white;
                    break;
                case "Ozymandias":
                    imageRight.sprite = s_Ozymandias;
                    imageLeft.color = Color.black;
                    imageRight.color = Color.white;
                    break;
                case "Volgar":
                    imageLeft.sprite = s_Volgar;
                    imageRight.color = Color.black;
                    imageLeft.color = Color.white;
                    break;
                case "Volo":
                    imageRight.sprite = s_Volo;
                    imageLeft.color = Color.black;
                    imageRight.color = Color.white;
                    break;
                default:
                    imageLeft.color = Color.black;
                    imageRight.color = Color.black;
                    break;
            }

            if (line == null)
            {
                file.Close();
                fileNames.Close();
                if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Taberna 1"))
                {
                    SceneManager.LoadScene("Scene");
                } else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Scene 2"))
                {
                    SceneManager.LoadScene("KenkuFight");
                }
            }
            switch (line)
            {
                case "---SUITE---":
                    background.sprite = s_Suite;
                    break;
                case "---MUELLES---":
                    background.sprite = s_Muelle;
                    break;
                default:
                    text.text = line;
                    name.text = nameLine;
                    break;
            }
            counter++;  
        }
    }
}
