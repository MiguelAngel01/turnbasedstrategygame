﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GuiController : MonoBehaviour
{
    
    private string line;
    private System.IO.StreamReader file;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NewGame()
    {
        SceneManager.LoadScene("Taberna 1");
    }

    public void Load()
    {
        file = new System.IO.StreamReader(@"Save.txt");
            
        line = file.ReadLine();

        SceneManager.LoadScene(line);
    }
    
    public void Exit()
	{
		Application.Quit();
	}
    
}
