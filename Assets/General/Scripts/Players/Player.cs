﻿using TbsFramework.Grid;
using UnityEngine;

namespace TbsFramework.Players
{
    public abstract class Player : MonoBehaviour
    {
        public int PlayerNumber;     
        public abstract void Play(CellGrid cellGrid);
    }
}